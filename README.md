# Django application

## Requirements

* Docker
* Docker-compose

The app's configuration is stored inside the _docker_compose.yml_ file.

## Running the containers
1. docker-compose build
2. docker-compose run

When you see the line ```Starting development server at http://0.0.0.0:8000/```, you can access the app using your browser at http(s)://localhost

## Managing Nginx

It is possible to either serve the app separately using HTTP and HTTPS or to redirect the connections received on the HTTP port to HTTPS.That is why the nginx.conf file has two server blocks - one for the HTTP protocol and the other for the HTTPS protocol.

The server block for the HTTPS protocol starts with the ``` listen 443 ssl;``` line and contains the configuration options used for managing the SSL certificates. Every server block contains a _location_ block, which configures the proxy to the Django application.

If you want to enable the HTTPS redirection, edit the nginx/nginx.conf file and uncomment the line ```return 301 https://$host$request_uri;```. You'll also need to comment out the _location_ block in the same server block. Also uncomment the last lines inside app/settings.py. Django will pick up the changes to the configuration on the fly, while the Nginx container needs to be restarted to pick up the updates to the nginx.conf file.
