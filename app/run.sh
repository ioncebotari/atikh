#!/bin/bash

sleep 15
python3 /opt/app/manage.py makemigrations
python3 /opt/app/manage.py migrate
echo "from django.contrib.auth.models import User; User.objects.create_superuser('admin', 'admin@example.com', 'pass')" | /opt/app/manage.py shell
python3 /opt/app/manage.py runserver 0.0.0.0:8000

