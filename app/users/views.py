from django.contrib import messages
from django.contrib.auth import login, authenticate, update_session_auth_hash
from django.contrib.auth.forms import PasswordChangeForm
from django.shortcuts import render, redirect
from django.http import HttpResponseForbidden
from users.forms import UserForm
from users.models import ChangedPass


def home(request):
    if request.user.is_authenticated:
        if hasattr(request.user, 'changedpass'):
            if request.method == 'POST':
                form = UserForm(request.POST, instance=request.user)
                # check whether it's valid:
                if form.is_valid():
                    form.save()
                    return redirect('/home/')
            else:
                form = UserForm()
                return render(request,'profile.html',{ 'nbar':'profile', 'form': form })
        else:
            return redirect('/password_change/')
    else:
        return redirect('/')

def change_password(request):
    if request.user.is_authenticated and request.method == 'POST':
        form = PasswordChangeForm(request.user, request.POST)
        if form.is_valid():
            user = form.save()
            update_session_auth_hash(request, user)  # Important!
            messages.success(request, 'Your password was successfully updated!')
            pc1 = ChangedPass(user=request.user, changed_password=1)
            pc1.save()
            return redirect('/home/')
        else:
            messages.error(request, 'Please correct the error below.')
    else:
        form = PasswordChangeForm(request.user).as_p()
    return render(request, 'password_change.html', {
        'form': form,
        'nbar': 'pchange'
    })


