from django.db import models
from django.contrib.auth.models import User

class ChangedPass(models.Model):
    user = models.OneToOneField(
        User,
        on_delete=models.CASCADE,
        primary_key=True,
    )
    changed_password = models.IntegerField(default=0)
